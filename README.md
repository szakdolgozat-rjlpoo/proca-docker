# Proca Docker

Docker Compose for Proca

## Defaults

Containers are extendable by modifying the correct file. 

### Frontend

Available on port 80.

### Supply gateway.

Available on port 8210.

Users are read from `supply-users.csv`. Password for the users is `test`.

### Product app

Database seeded from `init/product.sql`.

### Supply app

Database seeded from `init/supply.js`.
